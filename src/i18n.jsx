import i18n from "i18next";
import Backend from "i18next-xhr-backend";
import { reactI18nextModule } from "react-i18next";

const language = process.env.GATSBY_LANGUAGE || "en";

i18n
  .use(Backend)
  .use(reactI18nextModule)
  .init({
    fallbackLng: language,
    ns: ["translations"],
    defaultNS: "translations",
    debug: false,
    interpolation: {
      escapeValue: false
    },
    react: {
      wait: true
    }
  });

export default i18n;
