import React from "react";
import PropTypes from "prop-types";
import Image from "react-graceful-image";

const ImageGallery = ({ images }) => (
  <div>
    {Array.isArray(images) &&
      images.map(({ id, author }) => (
        <Image
          alt={author}
          key={id}
          src={`https://picsum.photos/200/300?image=${id}`}
        />
      ))}
  </div>
);

ImageGallery.propTypes = {
  images: PropTypes.arrayOf(PropTypes.shape)
};

ImageGallery.defaultProps = {
  images: []
};

export default ImageGallery;
