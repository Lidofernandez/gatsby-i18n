import React, { Component } from "react";
import PropTypes from "prop-types";
import { translate } from "react-i18next";
import ImageGallery from "../components/image_gallery";

class IndexPage extends Component {
  state = {
    isLoaded: false,
    items: []
  };

  componentDidMount = async () => {
    await fetch("https://picsum.photos/list")
      .then(response => response.json())
      .then(results =>
        this.setState({
          isLoaded: true,
          items: results
        })
      );
  };

  renderImages = () => {
    if (!this.state.isLoaded) {
      return <div>Loading...</div>;
    }

    return <ImageGallery images={this.state.items} />;
  };

  render() {
    return (
      <div>
        <h1>Hi people</h1>
        <p>{this.props.t("description")}</p>
        {this.renderImages()}
      </div>
    );
  }
}

IndexPage.propTypes = {
  t: PropTypes.func
};

IndexPage.defaultProps = {
  t: () => {}
};

export default translate("translations")(IndexPage);
